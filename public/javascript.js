var allbtn = document.querySelector(".allbtn");
var jsbtn = document.querySelector(".jsbtn");
var desc = document.querySelector(".desc");
var htmlsamples = document.querySelector(".htmlsamples");
var JSsamples = document.querySelector(".JSsamples");
JSsamples.style.display = "none";



//clicking jsbtn
jsbtn.onclick = function(){
  JSapps();
}

//clicking allbtn
allbtn.onclick = function(){
  htmlsamplesUp();
}

// function to display Javascript Apps
var JSapps = function(){
  htmlsamples.style.display = "none";
  JSsamples.style.display = "flex";
  desc.textContent = "Javascript Apps projects.";
}

// function to display htmlsamples
var htmlsamplesUp = function(){
  JSsamples.style.display = "none";
  htmlsamples.style.display = "flex";
  desc.textContent = "Projects powered by HTML, CSS & Javascript.";
}
